package org.erp.buying.database;

import java.util.HashMap;
import java.util.Map;

import org.erp.buying.model.Customer;
import org.erp.buying.model.Payment;
import org.erp.buying.model.Product;
import org.erp.buying.model.Purchase;
import org.erp.buying.model.PurchaseDetail;
import org.erp.buying.model.Supplier;
import org.erp.buying.model.sales.Sale;
import org.erp.buying.model.sales.SaleDetail;

public class DatabaseClass {
	
	private static Map<Long, Supplier> suppliers = new HashMap<>();	
	private static Map<Long, Customer> customers = new HashMap<>();	
	private static Map<Long, Product> products = new HashMap<>();
	private static Map<Long, Purchase> purchases = new HashMap<>();
	private static Map<Long, PurchaseDetail> purchaseDetails = new HashMap<>();
	private static Map<Long, Sale> sales = new HashMap<>();
	private static Map<Long, SaleDetail> saleDetails = new HashMap<>();
	private static Map<Long, Payment> payments = new HashMap<>();
	
	public static Map<Long, Supplier> getSuppliers() {
		return suppliers;
	}
	
	public static Map<Long, Customer> getCustomers() {
		return customers;
	}
	
	public static Map<Long, Product> getProducts() {
		return products;
	}
	
	public static Map<Long, Purchase> getPurchases() {
		return purchases;
	}
	
	public static Map<Long, PurchaseDetail> getPurchaseDetails() {
		return purchaseDetails;
	}
	
	public static Map<Long, Sale> getSales() {
		return sales;
	}
	
	public static Map<Long, SaleDetail> getSaleDetails() {
		return saleDetails;
	}
	
	public static Map<Long, Payment> getPayments() {
		return payments;
	}

}
