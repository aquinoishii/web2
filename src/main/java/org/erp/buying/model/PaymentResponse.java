package org.erp.buying.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class PaymentResponse {
	
	private long customerId;
	private String customerName;
	private float amount;
	private Date paymentDate;
	
	public PaymentResponse() {
		
	}

	public PaymentResponse(long customerId, String customerName, float amount, Date paymentDate) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.amount = amount;
		this.paymentDate = paymentDate;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}	

}
