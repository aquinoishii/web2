package org.erp.buying.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class PaymentRequest {
	
	private long customerId;
	private float amount;
	private Date paymentDate;
	
	public PaymentRequest() {
		
	}

	public PaymentRequest(long customerId, float amount, Date paymentDate) {
		super();
		this.customerId = customerId;
		this.amount = amount;
		this.paymentDate = paymentDate;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}	

}
