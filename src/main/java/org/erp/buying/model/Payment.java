package org.erp.buying.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class Payment {
	
	private long id;	
	private Customer customer;
	private float amount;
	private Date paymentDate;
	
	public Payment() {
		
	}
	
	public Payment(long id, Customer customer, float amount, Date paymentDate) {
		super();
		this.id = id;
		this.customer = customer;
		this.amount = amount;
		this.paymentDate = paymentDate;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}	
	
}
