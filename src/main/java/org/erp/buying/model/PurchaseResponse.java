package org.erp.buying.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PurchaseResponse {
	
	private long supplierId;
	private String supplierName;
	
	private List<PurchaseDetailResponse> details;

	
	public PurchaseResponse() {
		
	}
	
	public PurchaseResponse(long supplierId, String supplierName, List<PurchaseDetailResponse> details) {
		super();
		this.supplierId = supplierId;
		this.supplierName = supplierName;
		this.details = details;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public List<PurchaseDetailResponse> getDetails() {
		return details;
	}

	public void setDetails(List<PurchaseDetailResponse> details) {
		this.details = details;
	}	

}
