package org.erp.buying.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class PurchaseDetail {
	
	private long id;
	
	private Purchase purchase;
	private Product product;
	
	private float unitPrice;
	private float quantity;
	private float totalPrice;
	
	public PurchaseDetail() {

	}	
	
	public PurchaseDetail(long id, Purchase purchase, Product product, float unitPrice, float quantity) {
		super();
		this.id = id;
		this.purchase = purchase;
		this.product = product;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.totalPrice = unitPrice * quantity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Purchase getPurchase() {
		return purchase;
	}
	
	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public float getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public float getQuantity() {
		return quantity;
	}
	
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	
	public float getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
