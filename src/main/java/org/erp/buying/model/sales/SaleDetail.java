package org.erp.buying.model.sales;

import javax.xml.bind.annotation.XmlRootElement;

import org.erp.buying.model.Product;

@XmlRootElement()
public class SaleDetail {
	
	private long id;
	
	private Sale sale;
	private Product product;
	
	private float unitPrice;
	private float quantity;
	private float totalPrice;
	
	public SaleDetail() {
		
	}

	public SaleDetail(long id, Sale sale, Product product, float unitPrice, float quantity) {
		super();
		this.id = id;
		this.sale = sale;
		this.product = product;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.totalPrice = unitPrice * quantity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}	

}
