package org.erp.buying.model.sales;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class SaleDetailRequest {
	
	private long productId;
	private float quantity;
	private float unitPrice;
	
	public SaleDetailRequest () {
		
	}

	public SaleDetailRequest(long productId, float quantity, float unitPrice) {
		super();
		this.productId = productId;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}	

}
