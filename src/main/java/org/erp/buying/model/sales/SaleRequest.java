package org.erp.buying.model.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class SaleRequest {
	
	private String saleDescription;	
	private long customerId;
	
	@XmlElement(name = "saleDetails")
	private List<SaleDetailRequest> saleDetailRequests;	
	
	public SaleRequest() {
		
	}

	public SaleRequest(String saleDescription, long customerId, List<SaleDetailRequest> saleDetailRequests) {
		super();
		this.saleDescription = saleDescription;
		this.customerId = customerId;
		this.saleDetailRequests = saleDetailRequests;
	}

	public String getSaleDescription() {
		return saleDescription;
	}

	public void setSaleDescription(String saleDescription) {
		this.saleDescription = saleDescription;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public List<SaleDetailRequest> getSaleDetailRequests() {
		return saleDetailRequests;
	}

	public void setSaleDetailRequests(List<SaleDetailRequest> saleDetailRequests) {
		this.saleDetailRequests = saleDetailRequests;
	}	

}
