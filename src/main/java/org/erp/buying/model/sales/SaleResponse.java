package org.erp.buying.model.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SaleResponse {
	
	private long customerId;
	private String customerName;
	private float cost;
	
	private List<SaleDetailResponse> details;
	
	public SaleResponse() {
		
	}

	public SaleResponse(long customerId, String customerName, float cost, List<SaleDetailResponse> details) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.cost = cost;
		this.details = details;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}	

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public List<SaleDetailResponse> getDetails() {
		return details;
	}

	public void setDetails(List<SaleDetailResponse> details) {
		this.details = details;
	}	

}
