package org.erp.buying.model.sales;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SaleDetailResponse {
	
	private long productId;
	private String productName;
	private float unitPrice;
	private float quantity;
	private float totalPrice;
	
	public SaleDetailResponse() {
		
	}

	public SaleDetailResponse(long productId, String productName, float unitPrice, float quantity) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.totalPrice = unitPrice * quantity;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public float getQuantity() {
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
		
}
