package org.erp.buying.model.sales;

import javax.xml.bind.annotation.XmlRootElement;

import org.erp.buying.model.Customer;

@XmlRootElement
public class Sale {
	
	private long id;
	private String description;
	private Customer customer;
	private float cost;
	
	public Sale() {
		
	}

	public Sale(long id, String description, Customer customer) {
		super();
		this.id = id;
		this.description = description;
		this.customer = customer;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}	

}
