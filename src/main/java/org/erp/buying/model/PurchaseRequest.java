package org.erp.buying.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class PurchaseRequest {
	
	private String purchaseDescription;	
	private long supplierId;
	
	@XmlElement(name = "purchaseDetails")
	private List<PurchaseDetailRequest> purchaseDetailRequests;
	
	public PurchaseRequest() {
		
	}

	public PurchaseRequest(String purchaseDescription, long supplierId, List<PurchaseDetailRequest> purchaseDetailRequests) {
		super();
		this.purchaseDescription = purchaseDescription;
		this.supplierId = supplierId;
		this.purchaseDetailRequests = purchaseDetailRequests;
	}

	public String getPurchaseDescription() {
		return purchaseDescription;
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchaseDescription = purchaseDescription;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public List<PurchaseDetailRequest> getPurchaseDetailRequests() {
		return purchaseDetailRequests;
	}

	public void setPurchaseDetailRequests(List<PurchaseDetailRequest> purchaseDetailRequests) {
		this.purchaseDetailRequests = purchaseDetailRequests;
	}
	
	
}
