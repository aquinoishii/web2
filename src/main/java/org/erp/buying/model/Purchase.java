package org.erp.buying.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class Purchase {
	
	private long id;
	private String description;
	private Supplier supplier;
	
	public Purchase() {

	}

	public Purchase(long id, String description, Supplier supplier) {
		super();
		this.id = id;
		this.description = description;
		this.supplier = supplier;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return description;
	}

	public void setName(String description) {
		this.description = description;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
}
