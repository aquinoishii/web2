package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Supplier;


public class SupplierService {
	
	private Map<Long, Supplier> suppliers = DatabaseClass.getSuppliers();
	
	public SupplierService() {

	}

	public List<Supplier> getAllSuppliers() {
		return new ArrayList<Supplier>(suppliers.values());
	}
	
	public Supplier getSupplier(long id) {
		return suppliers.get(id);
	}
	
	public Supplier addSupplier(Supplier supplier) {
		supplier.setId(suppliers.size() + 1);
		suppliers.put(supplier.getId(), supplier);
		return supplier;
	}
	
	public Supplier updateSupplier(Supplier supplier) {
		suppliers.put(supplier.getId(), supplier);
		return supplier;
	}
	
	public void removeSupplier(long id) {		
		suppliers.remove(id);
	}
}
