package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Product;
import org.erp.buying.model.Purchase;
import org.erp.buying.model.PurchaseDetail;
import org.erp.buying.model.PurchaseDetailRequest;
import org.erp.buying.model.PurchaseDetailResponse;
import org.erp.buying.model.PurchaseRequest;
import org.erp.buying.model.PurchaseResponse;
import org.erp.buying.model.Supplier;

public class PurchaseService {
	
	private Map<Long, Purchase> purchases = DatabaseClass.getPurchases();
	private Map<Long, PurchaseDetail> purchaseDetails = DatabaseClass.getPurchaseDetails();
	private Map<Long, Supplier> suppliers = DatabaseClass.getSuppliers();
	private Map<Long, Product> products = DatabaseClass.getProducts();

	public List<PurchaseResponse> getAllPurchases() {
		
		List<PurchaseResponse> purchaseResponses = new ArrayList<>();
		
		for(Purchase purchase : purchases.values()) {
			
			PurchaseResponse purchaseResponse = new PurchaseResponse();
			purchaseResponse.setSupplierId(purchase.getSupplier().getId());
			purchaseResponse.setSupplierName(purchase.getSupplier().getName());
			
			List<PurchaseDetailResponse> details = new ArrayList<>();
			
			for(PurchaseDetail detail : purchaseDetails.values()) {
				if (detail.getPurchase().getId() == purchase.getId()) {
					
					PurchaseDetailResponse purchaseDetailResponse = new PurchaseDetailResponse();
					
					purchaseDetailResponse.setProductId(detail.getProduct().getId());
					purchaseDetailResponse.setProductName(detail.getProduct().getName());				
					purchaseDetailResponse.setUnitPrice(detail.getUnitPrice());
					purchaseDetailResponse.setQuantity(detail.getQuantity());
					purchaseDetailResponse.setTotalPrice(detail.getTotalPrice());
					
					details.add(purchaseDetailResponse);
					
				}
			}
			
			purchaseResponse.setDetails(details);
			purchaseResponses.add(purchaseResponse);
		}
		
		return purchaseResponses;
	}
	
	public PurchaseResponse getPurchase(long id) {		
				
		Purchase purchase = purchases.get(id);
		
		PurchaseResponse purchaseResponse = new PurchaseResponse();		
		purchaseResponse.setSupplierId(purchase.getSupplier().getId());
		purchaseResponse.setSupplierName(purchase.getSupplier().getName());
		
		List<PurchaseDetailResponse> details = new ArrayList<>();		
				
		for(PurchaseDetail detail : purchaseDetails.values()) {
			
			if (detail.getPurchase().getId() == id) {
				
				PurchaseDetailResponse purchaseDetailResponse = new PurchaseDetailResponse();
				
				purchaseDetailResponse.setProductId(detail.getProduct().getId());
				purchaseDetailResponse.setProductName(detail.getProduct().getName());				
				purchaseDetailResponse.setUnitPrice(detail.getUnitPrice());
				purchaseDetailResponse.setQuantity(detail.getQuantity());
				purchaseDetailResponse.setTotalPrice(detail.getTotalPrice());
				
				details.add(purchaseDetailResponse);
			}
		}
		
		purchaseResponse.setDetails(details);		
		
		return purchaseResponse;
	}
	
	public Purchase addPurchase(PurchaseRequest purchaseRequest) { 
		
		long purchaseId = purchases.size() + 1;		
		String purchaseDescription = purchaseRequest.getPurchaseDescription();		
		long supplierId = purchaseRequest.getSupplierId();
		
		Purchase purchase = new Purchase(purchaseId, purchaseDescription, suppliers.get(supplierId));
		purchases.put(purchase.getId(), purchase);
		
		
		List<PurchaseDetailRequest> details = purchaseRequest.getPurchaseDetailRequests();
		long purchaseDetailId;	 
		
		for (PurchaseDetailRequest detail : details) {
			
			purchaseDetailId = purchaseDetails.size() + 1;
			
			PurchaseDetail purchaseDetail = new PurchaseDetail(
				purchaseDetailId,	
				purchases.get(purchaseId), 
				products.get(detail.getProductId()), 
				detail.getUnitPrice(), 
				detail.getQuantity()
			);
			
			purchaseDetails.put(purchaseDetail.getId(), purchaseDetail);
		}
		
		
		return purchase;
	}
	
}
