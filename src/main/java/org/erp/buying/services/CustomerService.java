package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Customer;

public class CustomerService {
	
	private Map<Long, Customer> customers = DatabaseClass.getCustomers();	
	
	public CustomerService() {

	}

	public List<Customer> getAllCustomers() {
		return new ArrayList<Customer>(customers.values());
	}
	
	public Customer getCustomer(long id) {
		return customers.get(id);
	}
	
	public Customer addCustomer(Customer customer) {
		customer.setId(customers.size() + 1);
		customer.setBalance(0.0f);
		customers.put(customer.getId(), customer);
		return customer;
	}
	
	public Customer updateCustomer(Customer customer) {
		customers.put(customer.getId(), customer);
		return customer;
	}
	
	public Customer removeCustomer(long id) {		
		return customers.remove(id);
	}

}
