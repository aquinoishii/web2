package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Customer;
import org.erp.buying.model.Product;
import org.erp.buying.model.sales.Sale;
import org.erp.buying.model.sales.SaleDetail;
import org.erp.buying.model.sales.SaleDetailRequest;
import org.erp.buying.model.sales.SaleDetailResponse;
import org.erp.buying.model.sales.SaleRequest;
import org.erp.buying.model.sales.SaleResponse;

public class SaleService {

	private Map<Long, Sale> sales = DatabaseClass.getSales();
	private Map<Long, SaleDetail> saleDetails = DatabaseClass.getSaleDetails();
	private Map<Long, Customer> customers = DatabaseClass.getCustomers();
	private Map<Long, Product> products = DatabaseClass.getProducts();

	public SaleService() {

	}

	public List<SaleResponse> getAllSales() {

		List<SaleResponse> saleResponses = new ArrayList<>();

		for (Sale sale : sales.values()) {

			SaleResponse saleResponse = new SaleResponse();
			saleResponse.setCustomerId(sale.getCustomer().getId());
			saleResponse.setCustomerName(sale.getCustomer().getName());
			saleResponse.setCost(sale.getCost());

			List<SaleDetailResponse> saleDetailResponses = new ArrayList<>();

			for (SaleDetail detail : saleDetails.values()) {

				if (detail.getSale().getId() == sale.getId()) {

					SaleDetailResponse saleDetailResponse = new SaleDetailResponse(detail.getProduct().getId(),
							detail.getProduct().getName(), detail.getUnitPrice(), detail.getQuantity());

					saleDetailResponses.add(saleDetailResponse);
				}
			}

			saleResponse.setDetails(saleDetailResponses);
			saleResponses.add(saleResponse);
		}

		return saleResponses;
	}

	public SaleResponse getSale(long id) {

		Sale sale = sales.get(id);

		SaleResponse saleResponse = new SaleResponse();
		saleResponse.setCustomerId(sale.getCustomer().getId());
		saleResponse.setCustomerName(sale.getCustomer().getName());
		saleResponse.setCost(sale.getCost());

		List<SaleDetailResponse> saleDetailResponses = new ArrayList<>();

		for (SaleDetail detail : saleDetails.values()) {

			if (detail.getSale().getId() == id) {

				SaleDetailResponse saleDetailResponse = new SaleDetailResponse(detail.getProduct().getId(),
						detail.getProduct().getName(), detail.getUnitPrice(), detail.getQuantity());

				saleDetailResponses.add(saleDetailResponse);
			}
		}

		saleResponse.setDetails(saleDetailResponses);
		return saleResponse;
	}

	public Sale addSale(SaleRequest saleRequest) {

		long saleId = sales.size() + 1;
		String saleDescription = saleRequest.getSaleDescription();
		float cost = 0.0f;
		
		long customerId = saleRequest.getCustomerId();
		float balance;

		Sale sale = new Sale(saleId, saleDescription, customers.get(customerId));
		sales.put(sale.getId(), sale);

		List<SaleDetailRequest> details = saleRequest.getSaleDetailRequests();
		long saleDetailId;

		for (SaleDetailRequest detail : details) {

			saleDetailId = saleDetails.size() + 1;

			SaleDetail saleDetail = new SaleDetail(saleDetailId, sales.get(saleId), products.get(detail.getProductId()),
					detail.getUnitPrice(), detail.getQuantity());			
			
			cost += saleDetail.getTotalPrice();
			saleDetails.put(saleDetail.getId(), saleDetail);
		}
		
		sales.get(saleId).setCost(cost);
		
		balance = customers.get(customerId).getBalance() + cost * -1.0f;		
		customers.get(customerId).setBalance(balance);

		return sales.get(saleId);
	}

}
