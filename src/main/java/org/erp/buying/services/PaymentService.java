package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Customer;
import org.erp.buying.model.Payment;
import org.erp.buying.model.PaymentRequest;
import org.erp.buying.model.PaymentResponse;

public class PaymentService {
	
	private Map<Long, Payment> payments = DatabaseClass.getPayments();
	private Map<Long, Customer> customers = DatabaseClass.getCustomers();
	
	public List<PaymentResponse> getAllPayments() {
		
		List<PaymentResponse> paymentResponses = new ArrayList<>();
		
		for(Payment payment : payments.values()) {
			
			PaymentResponse paymentResponse = new PaymentResponse(
					payment.getCustomer().getId(),
					payment.getCustomer().getName(),
					payment.getAmount(),
					payment.getPaymentDate()
			);
			
			paymentResponses.add(paymentResponse);			
		}		
		
		return paymentResponses;
	}
	
	public PaymentResponse getPayment(long id) {
		
		Payment payment = payments.get(id);
		PaymentResponse paymentResponse = new PaymentResponse(
				payment.getCustomer().getId(),
				payment.getCustomer().getName(),
				payment.getAmount(),
				payment.getPaymentDate()
		);		
		
		return paymentResponse;
	}
	
	public Payment addPayment(PaymentRequest paymentRequest) {
		
		long paymentRequestId = payments.size() + 1;
		long customerId = paymentRequest.getCustomerId();		
		float customerBalance = customers.get(customerId).getBalance();
		
		Payment payment = new Payment(
				paymentRequestId,
				customers.get(customerId),
				paymentRequest.getAmount(),
				paymentRequest.getPaymentDate()
		);
		
		customerBalance += payment.getAmount();		
		customers.get(customerId).setBalance(customerBalance);
		
		payments.put(paymentRequestId, payment);		
		return payment;
	}
	
}