package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.PurchaseDetail;

public class PurchaseDetailService {
	
	private Map<Long, PurchaseDetail> purchaseDetails = DatabaseClass.getPurchaseDetails();
	
	public PurchaseDetailService() {

	}

	public List<PurchaseDetail> getAllPurchaseDetails() {
		return new ArrayList<PurchaseDetail>(purchaseDetails.values());
	}
	
	public PurchaseDetail getPurchaseDetail(long id) {
		return purchaseDetails.get(id);
	}
	
	public PurchaseDetail addPurchaseDetail(PurchaseDetail purchaseDetail) {
		purchaseDetail.setId(purchaseDetails.size() + 1);
		purchaseDetails.put(purchaseDetail.getId(), purchaseDetail);
		return purchaseDetail;
	}
	
	public PurchaseDetail updatePurchaseDetail(PurchaseDetail purchaseDetail) {
		purchaseDetails.put(purchaseDetail.getId(), purchaseDetail);
		return purchaseDetail;
	}
	
	public void removePurchaseDetail(long id) {		
		purchaseDetails.remove(id);
	}

}
