package org.erp.buying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.erp.buying.database.DatabaseClass;
import org.erp.buying.model.Product;

public class ProductService {

	private Map<Long, Product> products = DatabaseClass.getProducts();
	
	public ProductService() {

	}

	public List<Product> getAllProducts() {
		return new ArrayList<Product>(products.values());
	}
	
	public Product getProduct(long id) {
		return products.get(id);
	}
	
	public Product addProduct(Product product) {
		product.setId(products.size() + 1);
		products.put(product.getId(), product);
		return product;
	}
	
	public Product updateProduct(Product product) {
		products.put(product.getId(), product);
		return product;
	}
	
	public void removeProduct(long id) {		
		products.remove(id);
	}
	
}
