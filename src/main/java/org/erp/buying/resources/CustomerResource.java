package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.Customer;
import org.erp.buying.services.CustomerService;

@Path("/customers")
public class CustomerResource {
	
	CustomerService customerService = new CustomerService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Customer> getCustomers() {
		return customerService.getAllCustomers();
	}
	
	@GET
	@Path("/{customerId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public Customer getCustomer(@PathParam("customerId") long customerId) {
		return customerService.getCustomer(customerId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Customer addCustomer(Customer customer) {
		return customerService.addCustomer(customer);
	}
	
	@PUT
	@Path("/{customerId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Customer updateCustomer(@PathParam("customerId") long customerId, Customer customer) {		
		customer.setId(customerId);
		return customerService.updateCustomer(customer);
	}
	
	@DELETE
	@Path("/{customerId}")	
	@Produces(MediaType.APPLICATION_JSON)
	public void removeCustomer(@PathParam("customerId") long customerId) {
		customerService.removeCustomer(customerId);
	}		

}
