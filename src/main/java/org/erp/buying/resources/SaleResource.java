package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.sales.Sale;
import org.erp.buying.model.sales.SaleRequest;
import org.erp.buying.model.sales.SaleResponse;
import org.erp.buying.services.SaleService;

@Path("/sales")
public class SaleResource {
	
	SaleService saleService = new SaleService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SaleResponse> getSales() {
		return saleService.getAllSales();
	}
	
	@GET
	@Path("/{saleId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public SaleResponse getSale(@PathParam("saleId") long saleId) {
		return saleService.getSale(saleId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Sale addPurchase(SaleRequest saleRequest) {
		return saleService.addSale(saleRequest);
	}

}
