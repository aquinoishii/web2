package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.Product;
import org.erp.buying.services.ProductService;

@Path("/products")
public class ProductResource {
	
	ProductService productService = new ProductService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProducts() {
		return productService.getAllProducts();
	}
	
	@GET
	@Path("/{productId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public Product getProduct(@PathParam("productId") long productId) {
		return productService.getProduct(productId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product addProduct(Product product) {
		return productService.addProduct(product);
	}
	
	@PUT
	@Path("/{productId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Product updateProduct(@PathParam("productId") long productId, Product product) {		
		product.setId(productId);
		return productService.updateProduct(product);
	}
	
	@DELETE
	@Path("/{productId}")	
	@Produces(MediaType.APPLICATION_JSON)
	public void removeProduct(@PathParam("productId") long productId) {
		productService.removeProduct(productId);
	}

}
