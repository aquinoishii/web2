package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.Supplier;
import org.erp.buying.services.SupplierService;

@Path("/suppliers")
public class SupplierResource {
	
	SupplierService supplierService = new SupplierService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Supplier> getSuppliers() {
		return supplierService.getAllSuppliers();
	}
	
	@GET
	@Path("/{supplierId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public Supplier getSupplier(@PathParam("supplierId") long supplierId) {
		return supplierService.getSupplier(supplierId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Supplier addSupplier(Supplier supplier) {
		return supplierService.addSupplier(supplier);
	}
	
	@PUT
	@Path("/{supplierId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Supplier updateSupplier(@PathParam("supplierId") long supplierId, Supplier supplier) {		
		supplier.setId(supplierId);
		return supplierService.updateSupplier(supplier);
	}
	
	@DELETE
	@Path("/{supplierId}")	
	@Produces(MediaType.APPLICATION_JSON)
	public void removeSupplier(@PathParam("supplierId") long supplierId) {
		supplierService.removeSupplier(supplierId);
	}	
}
