package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.Payment;
import org.erp.buying.model.PaymentRequest;
import org.erp.buying.model.PaymentResponse;
import org.erp.buying.services.PaymentService;

@Path("/payments")
public class PaymentResource {
	
	PaymentService paymentService = new PaymentService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PaymentResponse> getPayments() {
		return paymentService.getAllPayments();
	}
	
	@GET
	@Path("/{paymentId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public PaymentResponse getPayment(@PathParam("paymentId") long paymentId) {
		return paymentService.getPayment(paymentId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Payment addPayment(PaymentRequest paymentRequest) {
		return paymentService.addPayment(paymentRequest);
	}
	
}
