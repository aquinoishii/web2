package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.PurchaseDetail;
import org.erp.buying.services.PurchaseDetailService;

@Path("/purchaseDetails")
public class PurchaseDetailResource {
	
	PurchaseDetailService purchaseDetailService = new PurchaseDetailService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PurchaseDetail> getPurchaseDetails() {
		return purchaseDetailService.getAllPurchaseDetails();
	}

}
