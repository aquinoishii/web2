package org.erp.buying.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.erp.buying.model.Purchase;
import org.erp.buying.model.PurchaseRequest;
import org.erp.buying.model.PurchaseResponse;
import org.erp.buying.services.PurchaseService;

@Path("/purchases")
public class PurchaseResource {
	
	PurchaseService purchaseService = new PurchaseService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PurchaseResponse> getPurchases() {
		return purchaseService.getAllPurchases();
	}
	
	@GET
	@Path("/{purchaseId}")
	@Produces(MediaType.APPLICATION_JSON)	
	public PurchaseResponse getPurchase(@PathParam("purchaseId") long purchaseId) {
		return purchaseService.getPurchase(purchaseId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Purchase addPurchase(PurchaseRequest purchaseRequest) {
		return purchaseService.addPurchase(purchaseRequest);
	}
	
}
